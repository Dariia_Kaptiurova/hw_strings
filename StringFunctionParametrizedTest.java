import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;

class StringFunctionParametrizedTest {

    @ParameterizedTest
    @ValueSource(strings = {"Желтый лист плывет.", "У какого берега, цикада,", "Вдруг проснешься ты?"})
    void lengthOfShortestWordInAString_shouldReturnTheLengthOfTheShortestWordInAString(String myString) {
        String[] splitArray = myString.split(" ");

        Set set = new TreeSet<String>((o1, o2) -> o1.length() - o2.length());
        set.addAll(Arrays.asList(splitArray));

        String shortest = (String) set.toArray()[0];
        int expected = shortest.length();

        assertEquals(expected, StringFunction.lengthOfShortestWordInAString(myString));
        assertNotEquals(0, StringFunction.lengthOfShortestWordInAString(myString));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Желтый лист плывет.", "У какого берега, цикада,", "Вдруг проснешься ты?"})
    void exchangingSymbolsInWords_shouldReplaceThreeLastSymbolsInEachWord(String[] words) {
        String allWords = Arrays.toString(words);
        char[] newArray = allWords.replace(allWords.substring(3, 6), "$$$").
                replace(allWords.substring(10, 13), "$$$").
                replace(allWords.substring(17, 20), "$$$").
                replace(allWords.substring(24, 27), "$$$").
                toCharArray();

        assertEquals(newArray, StringFunction.exchangingSymbolsInWords(words));
        assertNotEquals(0, StringFunction.exchangingSymbolsInWords(words));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Желтый лист плывет.", "У какого берега, цикада,", "Вдруг проснешься ты?"})
    void addingPlacesInAString_shouldAddSpacesAfterPunctuationMarks(String myStringWithoutSpaces) {
        String stringWithSpaces = myStringWithoutSpaces.replace(",", ", ").
                replace(".", ". ").replace(":", ": ").
                replace(";", "; ").replace("!", "! ").
                replace("?", "? ").replace(")", ") ").
                replace("  ", " ");

        assertEquals(stringWithSpaces, StringFunction.addingPlacesInAString(myStringWithoutSpaces));
        assertNotEquals(0, StringFunction.addingPlacesInAString(myStringWithoutSpaces));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Желтый лист плывет.", "У какого берега, цикада,", "Вдруг проснешься ты?"})
    void deleteAllRepeatedSymbolsInAString_shouldReturnStringWithoutSameSymbols(String myString) {
        StringBuilder withoutRepeatedSymbols = new StringBuilder();
        LinkedHashSet<Character> temp = new LinkedHashSet<Character>();
        for (
                int i = 0; i < myString.length(); i++) {
            if (temp.add(myString.charAt(i))) {
                withoutRepeatedSymbols.append(myString.charAt(i));
            }
        }

        assertEquals(withoutRepeatedSymbols, StringFunction.deleteAllRepeatedSymbolsInAString(myString));
        assertNotEquals(0, StringFunction.deleteAllRepeatedSymbolsInAString(myString));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Желтый лист плывет.", "У какого берега, цикада,", "Вдруг проснешься ты?"})
    void amountOfWordsInAString_shouldCountWordsInAString(String myString) {
        String withoutSpaces = myString.replace(" ", "");
        int wordsCounting = myString.length() - withoutSpaces.length() + 1;

        assertEquals(wordsCounting, StringFunction.amountOfWordsInAString(String.valueOf(myString)));
        assertNotEquals(0, StringFunction.amountOfWordsInAString(String.valueOf(myString)));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Желтый лист плывет.", "У какого берега, цикада,", "Вдруг проснешься ты?"})
    void cutAString_shouldCutAPieceOfString(StringBuilder reversedString) {
        int length = 6;
        StringBuilder stringWithoutPiece = new StringBuilder();
        for (int i = 0; i < length; i++) {
            stringWithoutPiece = reversedString.deleteCharAt(5);
        }

        assertEquals(reversedString, StringFunction.cutAString(reversedString));
        assertNotEquals(0, StringFunction.cutAString(reversedString));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Желтый лист плывет.", "У какого берега, цикада,", "Вдруг проснешься ты?"})
    void reverseGivenString_shouldReturnReversedString(StringBuilder reversedString) {
        reversedString.reverse();

        assertEquals(reversedString, StringFunction.reverseGivenString(reversedString));
        assertNotEquals(0, StringFunction.reverseGivenString(reversedString));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Желтый лист плывет.", "У какого берега, цикада,", "Вдруг проснешься ты?"})
    void deleteLastWordInAString_shouldCutLastWordInString(StringBuilder reversedString) {
        reversedString.reverse();
        StringBuilder cutLastWordInAString = new StringBuilder
                (reversedString.substring(reversedString.indexOf(" ")));
        cutLastWordInAString.reverse();

        assertEquals(cutLastWordInAString, StringFunction.deleteLastWordInAString(reversedString.reverse()));
        assertNotEquals(0, StringFunction.deleteLastWordInAString(reversedString.reverse()));
    }
}