import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class StringFunctionTest {

    @Test
    public void lengthOfShortestWordInAString_shouldReturnTheLengthOfTheShortestWordInAStringPass() {

        //given
        String myString = "Quantity of words:...";

        //when
        String[] splitArray = myString.split(" ");

        Set set = new TreeSet<String>((o1, o2) -> o1.length() - o2.length());
        set.addAll(Arrays.asList(splitArray));

        String shortest = (String) set.toArray()[0];
        int expected = shortest.length();

        //then
        assertEquals(expected, StringFunction.lengthOfShortestWordInAString(myString));
    }

    @Test
    public void lengthOfShortestWordInAString_shouldReturnTheLengthOfTheShortestWordInAStringFailed() {

        //given
        String myString = "Quantity of words:...";
        int expected = 0;

        //when
        String[] splitArray = myString.split(" ");

        Set set = new TreeSet<String>((o1, o2) -> o1.length() - o2.length());
        set.addAll(Arrays.asList(splitArray));

        String shortest = (String) set.toArray()[0];

        //then
        assertNotEquals(10, StringFunction.lengthOfShortestWordInAString(myString));
    }


    @Test
    public void exchangingSymbolsInWords_shouldReplaceThreeLastSymbolsInEachWordPass() {

        //given
        String[] words = {"Актер", "Амвон", "Ареал", "Алиcа"};
        int lengthOfWord = 5;

        //when
        String[] expected = {"Ак$$$", "Ам$$$", "Ар$$$", "Ал$$$"};

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(StringFunction.exchangingSymbolsInWords(words, lengthOfWord)));
    }

    @Test
    public void exchangingSymbolsInWords_shouldReplaceThreeLastSymbolsInEachWordFailed() {

        //given
        String[] words = {"Актер", "Амвон", "Ареал", "Алиса"};
        int lengthOfWord = 5;

        //when
        String expected = null;

        //then
        assertNotEquals(expected, Arrays.toString(StringFunction.exchangingSymbolsInWords(words, lengthOfWord)));
    }


    @Test
    public void addingPlacesInAString_shouldAddSpacesAfterPunctuationMarksPass() {

        //given
        String myStringWithoutSpaces = ("Quantity,of, words:");

        //when
        String stringWithSpaces = myStringWithoutSpaces.replace(",", ", ").
                replace(".", ". ").replace(":", ": ").
                replace(";", "; ").replace("!", "! ").
                replace("?", "? ").replace(")", ") ").
                replace("  ", " ");

        //then
        assertEquals(stringWithSpaces, StringFunction.addingPlacesInAString(myStringWithoutSpaces));
    }

    @Test
    public void addingPlacesInAString_shouldAddSpacesAfterPunctuationMarksFailed() {
        //given
        String myStringWithoutSpaces = ("Quantity,of, words:");

        //when
        String stringWithSpaces = myStringWithoutSpaces.replace(",", ", ").
                replace(".", ". ").replace(":", ": ").
                replace(";", "; ").replace("!", "! ").
                replace("?", "? ").replace(")", ") ").
                replace("  ", " ");

        //then
        assertNotEquals(0, StringFunction.addingPlacesInAString(myStringWithoutSpaces));
    }


    @Test
    public void deleteAllRepeatedSymbolsInAString_shouldReturnStringWithoutSameSymbolsPass() {

        //given
        String myString = "Quantity of words:...";

        //when
        String expected = "Quantiy ofwrds:.";

        //then
        assertEquals(expected, StringFunction.deleteAllRepeatedSymbolsInAString(myString).toString());
    }

    @Test
    public void deleteAllRepeatedSymbolsInAString_shouldReturnStringWithoutSameSymbolsFailed() {

        //given
        String myString = "Quantity of words:...";

        //when
        StringBuilder withoutRepeatedSymbols = new StringBuilder("Quantity of words:...");
        LinkedHashSet<Character> temp = new LinkedHashSet<Character>();
        for (
                int i = 0; i < myString.length(); i++) {
            if (temp.add(myString.charAt(i))) {
                withoutRepeatedSymbols.append(myString.charAt(i));
            }
        }

        //then
        assertNotEquals("Quantity of ", StringFunction.deleteAllRepeatedSymbolsInAString(myString));
    }


    @Test
    public void amountOfWordsInAString_shouldCountWordsInAStringPass() {

        //given
        String myString = "Quantity of words:...";

        //when
        String withoutSpaces = myString.replace(" ", "");
        int wordsCounting = myString.length() - withoutSpaces.length() + 1;

        //then
        assertEquals(wordsCounting, StringFunction.amountOfWordsInAString(myString));
    }

    @Test
    public void amountOfWordsInAString_shouldCountWordsInAStringFailed() {

        //given
        String myString = "Quantity of words:...";

        //when
        String withoutSpaces = myString.replace(" ", "");
        int wordsCounting = myString.length() - withoutSpaces.length() + 1;

        //then
        assertNotEquals(0, StringFunction.amountOfWordsInAString(String.valueOf(wordsCounting)));
    }


    @Test
    public void cutAString_shouldCutAPieceOfStringPass() {

        //given
        int length = 6;
        StringBuilder reversedString = new StringBuilder("Quantity of words:...");

        //when
        StringBuilder stringWithoutPiece = new StringBuilder();
        for (int i = 0; i < length; i++) {
            stringWithoutPiece = reversedString.deleteCharAt(5);
        }

        //then
        assertEquals(stringWithoutPiece, StringFunction.cutAString(reversedString));
    }

    @Test
    public void cutAString_shouldCutAPieceOfStringFailed() {

        //given
        int length = 6;
        StringBuilder reversedString = new StringBuilder("Quantity of words:...");

        //when
        StringBuilder stringWithoutPiece = new StringBuilder();
        for (int i = 0; i < length; i++) {
            stringWithoutPiece = reversedString.deleteCharAt(5);
        }

        //then
        assertNotEquals(0, StringFunction.cutAString(reversedString));
    }


    @Test
    public void reverseGivenString_shouldReturnReversedStringPass() {

        //given
        StringBuilder reversedString = new StringBuilder("Quantity of words:...");

        //when
        reversedString.reverse();

        //then
        assertEquals(reversedString, StringFunction.reverseGivenString(reversedString));
    }

    @Test
    public void reverseGivenString_shouldReturnReversedStringFailed() {

        //given
        StringBuilder reversedString = new StringBuilder("Quantity of words:...");

        //when
        reversedString.reverse();

        //then
        assertNotEquals("Quantity of words:...", StringFunction.reverseGivenString(reversedString));
    }


    @Test
    public void deleteLastWordInAString_shouldCutLastWordInStringPass() {

        //given
        StringBuilder reversedString = new StringBuilder("Quantity of words:...");

        //when
        String expected = "Quantity of ";

        //then
        assertEquals(expected, StringFunction.deleteLastWordInAString(reversedString).toString());
    }

    @Test
    public void deleteLastWordInAString_shouldCutLastWordInStringFailed() {
        //given
        StringBuilder reversedString = new StringBuilder("Quantity of words:...");

        //when
        reversedString.reverse();
        StringBuilder cutLastWordInAString = new StringBuilder
                (reversedString.substring(reversedString.indexOf(" ")));
        cutLastWordInAString.reverse();

        //then
        assertNotEquals(0, StringFunction.deleteLastWordInAString(reversedString));
    }
}