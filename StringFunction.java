import java.util.*;

public class StringFunction {

    public static int lengthOfShortestWordInAString(String myString) {

        String[] splitArray = myString.split(" ");

        Set set = new TreeSet<String>((o1, o2) -> o1.length() - o2.length());
        set.addAll(Arrays.asList(splitArray));

        String shortest = (String) set.toArray()[0];
        return shortest.length();
    }


    public static String[] exchangingSymbolsInWords(String[] words, int lengthOfWord) {

        for (int i = 0; i < words.length; i++) {
            if (words[i].length() == lengthOfWord) {
                int temp = lengthOfWord - 3;
                words[i] = words[i].substring(0, temp) + "$$$";
            }
        }
        return words;
    }


    public static String addingPlacesInAString(String myStringWithoutSpaces) {

        String stringWithSpaces = myStringWithoutSpaces.replace(",", ", ").
                replace(".", ". ").replace(":", ": ").
                replace(";", "; ").replace("!", "! ").
                replace("?", "? ").replace(")", ") ").
                replace("  ", " ");
        return stringWithSpaces;
    }


    public static StringBuilder deleteAllRepeatedSymbolsInAString(String myString) {

        StringBuilder withoutRepeatedSymbols = new StringBuilder();
        LinkedHashSet<Character> temp = new LinkedHashSet<Character>();
        for (
                int i = 0; i < myString.length(); i++) {
            if (temp.add(myString.charAt(i))) {
                withoutRepeatedSymbols.append(myString.charAt(i));
            }
        }
        return withoutRepeatedSymbols;
    }


    public static int amountOfWordsInAString(String myString) {

        String withoutSpaces = myString.replace(" ", "");
        int wordsCounting = myString.length() - withoutSpaces.length() + 1;
        return wordsCounting;
    }


    public static StringBuilder cutAString(StringBuilder reversedString) {

        int length = 7;
        StringBuilder stringWithoutPiece = new StringBuilder();
        for (int i = 0; i < length; i++) {
            stringWithoutPiece = reversedString.deleteCharAt(5);
        }
        return stringWithoutPiece;
    }


    public static StringBuilder reverseGivenString(StringBuilder reversedString) {
        return reversedString.reverse();
    }


    public static StringBuilder deleteLastWordInAString(StringBuilder reversedString) {
        reversedString.reverse();
        StringBuilder cutLastWordInAString = new StringBuilder
                (reversedString.substring(reversedString.indexOf(" ")));
        return cutLastWordInAString.reverse();
    }


    public static void main(String[] args) {

        System.out.println("Length of the shortest word in a string: "
                + lengthOfShortestWordInAString("Дана строка, состоящая из слов..."));
        System.out.println("Change last 3 symbols in every word: "
                + Arrays.toString(exchangingSymbolsInWords(new String[]{"Адепт", "Алмаз", "Апорт", "Астма"}, 5)));
        System.out.println("String with new places: "
                + addingPlacesInAString("Дана,строка, состоящая,из,слов"));
        System.out.println("String with unrepeatable symbols: "
                + deleteAllRepeatedSymbolsInAString("Дана строка, состоящая из слов..."));
        System.out.println("Amount of words in a string: "
                + amountOfWordsInAString("Дана строка, состоящая из слов..."));
        System.out.println("String without a piece: "
                + cutAString(new StringBuilder("Дана строка, состоящая из слов...")));
        System.out.println("Reversed string: "
                + reverseGivenString(new StringBuilder("Дана строка, состоящая из слов...")));
        System.out.println("String without last word: "
                + deleteLastWordInAString(new StringBuilder("Дана строка, состоящая из слов...")));
    }
}
