import junit.framework.Assert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class ConvertFunctionParametrizedTest {

    @ParameterizedTest
    @ValueSource(ints = {7, 13, 47, 93, 20})
    void convertIntegerNumberToString_shouldReturnIntegerNumberToString(int integerNumber) {
        String expected = Integer.toString(integerNumber);
        assertEquals(expected, ConvertFunction.convertIntegerNumberToString(integerNumber));
        assertNotEquals(0, ConvertFunction.convertIntegerNumberToString(integerNumber));
    }

    @ParameterizedTest
    @ValueSource(doubles = {7.13, 4.7, 9.3, 20.2})
    void convertRealNumberToString_shouldReturnRealNumberToString(double realNumber) {
        String expected = Double.toString(realNumber);
        assertEquals(expected, ConvertFunction.convertRealNumberToString(realNumber));
        assertNotEquals(0, ConvertFunction.convertRealNumberToString(realNumber));
    }

    @ParameterizedTest
    @ValueSource(strings = {"7", "13", "47", "93", "20"})
    void convertStringToInteger_shouldReturnStringToIntegerNumber(String numberInteger) {
        int expected = Integer.parseInt(numberInteger);
        assertEquals(expected, ConvertFunction.convertStringToInteger(numberInteger));
        assertNotEquals(0, ConvertFunction.convertStringToInteger(numberInteger));
    }

    @ParameterizedTest
    @ValueSource(strings = {"7.13", "4.7", "9.3", "20.2"})
    void convertStringToRealNumber_shouldReturnStringToRealNumber(String numberReal) {
        double expected = Double.parseDouble(numberReal);
        assertEquals(expected, ConvertFunction.convertStringToRealNumber(numberReal));
        assertNotEquals(0, ConvertFunction.convertStringToRealNumber(numberReal));
    }
}