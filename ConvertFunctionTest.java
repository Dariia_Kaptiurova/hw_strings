import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConvertFunctionTest {

    @Test
    public void shouldReturnIntegerNumberToStringPass() {

        //given
        int integerNumber1 = 13;
        int integerNumber2 = 0;
        int integerNumber3 = -13;

        //when
        String expected1 = Integer.toString(integerNumber1);
        String expected2 = Integer.toString(integerNumber2);
        String expected3 = Integer.toString(integerNumber3);

        //then
        assertEquals(expected1, ConvertFunction.convertIntegerNumberToString(integerNumber1));
        assertEquals(expected2, ConvertFunction.convertIntegerNumberToString(integerNumber2));
        assertEquals(expected3, ConvertFunction.convertIntegerNumberToString(integerNumber3));
    }

    @Test
    public void shouldReturnIntegerNumberToStringFailed() {

        //given
        int integerNumber = 13;

        //when
        String expected = Integer.toString(integerNumber);

        //then
        assertNotEquals(13, ConvertFunction.convertIntegerNumberToString(integerNumber));
    }

    @Test
    public void shouldReturnRealNumberToStringPass() {

        //given
        double realNumber1 = 1.3;
        double realNumber2 = 0;
        double realNumber3 = -1.3;

        //when
        String expected1 = Double.toString(realNumber1);
        String expected2 = Double.toString(realNumber2);
        String expected3 = Double.toString(realNumber3);

        //then
        assertEquals(expected1, ConvertFunction.convertRealNumberToString(realNumber1));
        assertEquals(expected2, ConvertFunction.convertRealNumberToString(realNumber2));
        assertEquals(expected3, ConvertFunction.convertRealNumberToString(realNumber3));
    }

    @Test
    public void shouldReturnRealNumberToStringFailed() {

        //given
        double realNumber = 1.3;

        //when
        String expected = Double.toString(realNumber);

        //then
        assertNotEquals(1.3, ConvertFunction.convertRealNumberToString(realNumber));
    }

    @Test
    public void shouldReturnStringToIntegerNumberPass() {

        //given
        String numberInteger1 = "13";
        String numberInteger2 = "0";
        String numberInteger3 = "-13";

        //when
        int expected1 = Integer.parseInt(numberInteger1);
        int expected2 = Integer.parseInt(numberInteger2);
        int expected3 = Integer.parseInt(numberInteger3);

        //then
        assertEquals(expected1, ConvertFunction.convertStringToInteger(numberInteger1));
        assertEquals(expected2, ConvertFunction.convertStringToInteger(numberInteger2));
        assertEquals(expected3, ConvertFunction.convertStringToInteger(numberInteger3));
    }

    @Test
    public void shouldReturnStringToIntegerNumberFailed() {

        //given
        String numberInteger = "13";

        //when
        int expected = Integer.parseInt(numberInteger);

        //then
        assertNotEquals("13", ConvertFunction.convertStringToInteger(numberInteger));
    }

    @Test
    public void shouldReturnStringToRealNumberPass() {

        //given
        String numberReal1 = "1.3";
        String numberReal2 = "0";
        String numberReal3 = "-1.3";

        //when
        double expected1 = Double.parseDouble(numberReal1);
        double expected2 = Double.parseDouble(numberReal2);
        double expected3 = Double.parseDouble(numberReal3);

        //then
        assertEquals(expected1, ConvertFunction.convertStringToRealNumber(numberReal1));
        assertEquals(expected2, ConvertFunction.convertStringToRealNumber(numberReal2));
        assertEquals(expected3, ConvertFunction.convertStringToRealNumber(numberReal3));
    }

    @Test
    public void shouldReturnStringToRealNumberFailed() {

        //given
        String numberReal = "1.3";

        //when
        double expected = Double.parseDouble(numberReal);

        //then
        assertNotEquals("1.3", ConvertFunction.convertStringToRealNumber(numberReal));
    }
}