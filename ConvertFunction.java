public class ConvertFunction {

    public static String convertIntegerNumberToString(int integerNumber) {
        String stringOfIntegerNumber = Integer.toString(integerNumber);
        return stringOfIntegerNumber;
    }

    public static String convertRealNumberToString(double realNumber) {
        String stringOfRealNumber = Double.toString(realNumber);
        return stringOfRealNumber;
    }

    public static int convertStringToInteger(String numberInteger) {
        int newIntegerNumber = Integer.parseInt(numberInteger);
        return newIntegerNumber;
    }

    public static double convertStringToRealNumber(String numberReal) {
        double newRealNumber = Double.parseDouble(numberReal);
        return newRealNumber;
    }

    public static void main(String[] args) {
        System.out.println(convertIntegerNumberToString(23974317));
        System.out.println(convertRealNumberToString(2397.4317));
        System.out.println(convertStringToInteger("713479320"));
        System.out.println(convertStringToRealNumber("7134.7932"));
    }
}
