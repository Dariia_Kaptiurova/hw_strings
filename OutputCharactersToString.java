public class OutputCharactersToString {

    public static void main(String[] args) {
        String string1 = "a b c d e f g h i j k l m n o p q r s t u v w x y z";
        System.out.println("English alphabet: " + string1.toUpperCase());

        StringBuilder string2 = new StringBuilder(string1);
        System.out.println("Reversed English alphabet: " + string2.reverse());

        String string3 = "а б в г д е ё ж з и й к л м н о п р с т у ф х ц ч ш щ ъ ы ь э ю я";
        System.out.println("Russian alphabet: " + string3);

        String string4 = "0 1 2 3 4 5 6 7 8 9";
        System.out.println("Numbers: " + string4);

        String string5a = "  ! \" # $ % & ' ( ) * + , - . / ";
        String string5b = " : ; < = > ? @ ";
        String string5c = " [ \\ ] ^ _ ` ";
        String string5d = " { | } ~";
        System.out.println("Printed symbols ASCII: " +
                string5a + string4 + string5b + string1.toUpperCase() + string5c + string1 + string5d);
    }
}

